<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use AppBundle\Lib\PrepareDataTrait;
use AppBundle\Entity\EmailQueue;

class MailCommand extends ContainerAwareCommand
{
    use PrepareDataTrait;
    
    protected $output;
    
    protected function configure()
    {
        $this
        ->setName('app:mail-send')
        ->setDescription('Wysyła maile.')
        ->setHelp('Ta komenda uruchamia wysyłkę maili ze statusem INPROGRESS.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->output->writeln('Skrypt do wysyłania maili.');
        
        $this->sendMessages(
            $this->getEmailQueue(),
            EmailQueue::MAILER_SERVICE_SMTP
        );
    }
    
    /**
     * 
     * @return array
     */
    protected function getEmailQueue()
    {   
        $doctrine = $this->getContainer()->get('doctrine');
        
        $repository = $doctrine
            ->getRepository('AppBundle:EmailQueue');
        
        $query = $repository
            ->createQueryBuilder('EmailQueue');
        
        $query->where('EmailQueue.status IN (:statuses)')
            ->setParameter('statuses', [EmailQueue::STATUS_INPROGRESS])
            ->orderBy('EmailQueue.priority', 'DESC')
            ->setMaxResults(EmailQueue::LIMIT_SEND);
        
        $emailQueue = $query->getQuery()->getResult();
        
        return $this->prepareData(
            $emailQueue
        );
    }
    
    protected function sendMessages($emails, $provider = EmailQueue::MAILER_SERVICE_SMTP)
    {
        $providerMethod = 'send' . ucfirst($provider);
        
        if (!empty($emails) && is_array($emails)) {
            foreach ($emails as $email) {
                $this->$providerMethod($email);
            }
        } else {
            $this->output->writeln('Brak maili do wysłania.');
        }
    }
    
    protected function sendSmtp($email)
    {
        $status = EmailQueue::STATUS_SENT;
        
        try {
            $message = \Swift_Message::newInstance()
                ->setSubject($email['subject'])
                ->setFrom($email['fromEmail'])
                ->setTo($email['email'])
                ->setBody(
                    $email['content'],
                    'text/html'
                );
            
            if (!empty($email['files']) && is_array($email['files'])) {
                $filesDirectory = $this->getContainer()->getParameter('files_directory');
                
                foreach ($email['files'] as $file) {
                    $path = $filesDirectory . DIRECTORY_SEPARATOR . $file;
                    
                    if (is_readable($path)) {
                        $message->attach(\Swift_Attachment::fromPath($path));
                    }
                }
            }
            
            $result = $this->getContainer()->get('mailer')->send($message, $failures);
            
            if (empty($failures)) {
                $this->output->writeln('Wysłałem maila na adres: ' . $email['email']);
            } else {
                $this->output->writeln('Wystapił błąd przy wysyłce maila maila na adres: ' . $email['email']);
                $status = EmailQueue::STATUS_ERROR;
            }
        } catch (\Exception $exc) {
            $this->output->writeln($exc->getMessage());
            $status = EmailQueue::STATUS_ERROR;
        }
        
        $this->updateStatus($email['id'], $status);
    }
    
    protected function sendMailchimp($email)
    {
        // TODO - implementation of mailchimp communication
    }
    
    /**
     * 
     * @param int $id
     * @param int $status
     */
    protected function updateStatus($id, $status) 
    {   
        $doctrine = $this->getContainer()->get('doctrine');
        
        $repository = $doctrine
            ->getRepository('AppBundle:EmailQueue');        
        
        $query = $repository
            ->createQueryBuilder('EmailQueue');
       
        $query->update('AppBundle:EmailQueue EmailQueue')
            ->set('EmailQueue.status', $status)
            ->where('EmailQueue.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->execute();
    }
}
