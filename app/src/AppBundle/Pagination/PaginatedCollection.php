<?php

namespace AppBundle\Pagination;

class PaginatedCollection
{
    private $items;

    private $total;

    private $count;
    
    private $links = [];

    public function __construct(array $items, $totalItems)
    {
        $this->items = $items;
        $this->total = $totalItems;
        $this->count = count($items);
    }
    
    public function addLink($type, $link) 
    {
        if (!empty($type) && !empty($link)) {
            $this->links[$type] = $link;
        }
    }
    
    public function getLinks() 
    {
        return $this->links;
    }
    
    public function getItems() 
    {
        return $this->items;
    }
    
    public function getCount() 
    {
        return $this->count;
    }
    
    public function getTotal() 
    {
        return $this->total;
    }
}
