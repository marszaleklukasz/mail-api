<?php

namespace AppBundle\Lib;

trait PrepareDataTrait {

    /**
     * 
     * @param array $data
     * @return array
     */
    protected function prepareData($data = null)
    {
        $result = [];
        
        if (!empty($data) && is_array($data)) {
            foreach ($data as $emailQueueE) {
                $result[] = [
                    'id' => $emailQueueE->getId(),
                    'fromEmail' => $emailQueueE->getFromEmail(),
                    'email' => $emailQueueE->getEmail(),
                    'subject' => $emailQueueE->getSubject(),
                    'status' => $emailQueueE->getStatus(),
                    'priority' => $emailQueueE->getPriority(),
                    'content' => $emailQueueE->getContent()->getContent(),
                    'files' => $this->prepareFiles($emailQueueE->getContent()->getFiles())
                ];
            }
        }
        
        return $result;
    }
    
    /**
     * 
     * @param \Traversable $files
     * @return array
     */
    protected function prepareFiles($files = null) 
    {
        $result = [];  
  
        if (!empty($files) && $files instanceof \Traversable) {
            foreach ($files as $fileE) {
                $result[] = $fileE->getName();
            }
        }
        
        return $result;
    }   
}
