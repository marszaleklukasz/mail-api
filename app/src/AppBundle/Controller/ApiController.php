<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Lib\PrepareDataTrait;
use AppBundle\Entity\EmailQueue;
use AppBundle\Entity\Content;
use AppBundle\Entity\File;

/**
 * @property EmailQueue $emailQueueE
 * @property Content $contentE
 * @property File $fileE
 */
class ApiController extends AppController
{   
    use PrepareDataTrait;
    
    /**
     * Matches /api exactly
     *
     * @Route("/api", name="api_list")
     */
    public function listAction(Request $request)
    {   
        return $this->listEmails($request, 'api_list');
    }
    
    /**
     * Matches /api/not-sent exactly
     *
     * @Route("/api/not-sent", name="api_list_not_sent")
     */
    public function listNotSentAction(Request $request)
    {   
        return $this->listEmails($request, 'api_list_not_sent', [EmailQueue::STATUS_NEW, EmailQueue::STATUS_INPROGRESS, EmailQueue::STATUS_ERROR]);
    }
    
    /**
     * Matches /api/show/*
     *
     * @Route("/api/show/{id}", name="api_show")
     */
    public function showAction($id)
    {   
        if (!is_numeric($id)) {
            throw new \Exception('Wystąpił błąd parametru!');
        }
        
        $emailQueueE = $this->getDoctrine()
            ->getRepository('AppBundle:EmailQueue')
            ->find($id);
        
        if (empty($emailQueueE)) {
            throw new \Exception('Brak rekordu o ID: ' . $id . '!');
        }
        
        $this->response->setData([
            'item' => [
                'id' => $emailQueueE->getId(),
                'fromEmail' => $emailQueueE->getFromEmail(),
                'email' => $emailQueueE->getEmail(),
                'subject' => $emailQueueE->getSubject(),
                'status' => $emailQueueE->getStatus(),
                'priority' => $emailQueueE->getPriority(),
                'content' => $emailQueueE->getContent()->getContent(),
                'files' => $this->prepareFiles($emailQueueE->getContent()->getFiles())
            ]
        ]);  
        
        return $this->response;
    }    
    
    /**
     * Matches /api/create exactly
     *
     * @Route("/api/create", name="api_create")
     */
    public function createAction(Request $request)
    {
        if ($request->getMethod() !== 'POST') {
            throw new HttpException(400, 'Nieprawidłowe zapytanie');
        }
        
        $content = $request->request->get('content');
        $emails = $request->request->get('email');
        
        if (!empty($content) && !empty($emails)) {
            $contentE = new Content();
            $contentE->setContent($content);
            
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($contentE);
            
            $em->flush();
            
            if (!empty($contentE->getId())) {
                $this->processFiles($request, $contentE);
                $ids = $this->saveEmailQueue($request, $contentE);
                
                if (!empty($ids)) {
                    $this->response->setData([
                        'status' => 'success',
                        'ID' => $ids
                    ]);
                    
                    return $this->response;
                }
            }
        }
        
        throw new \Exception('Wystąpił nieoczekiwany błąd!');
    }
    
    protected function processFiles(Request $request, $contentE) {
        $files =  $request->files->get('files');
        
        if (!empty($files) && is_array($files)) {
            foreach ($files as $file) {
                if ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile && empty($file->getError())) {
                    $filesDirectory = $this->getParameter('files_directory');

                    $fileName = md5(uniqid()).'.'.$file->guessExtension();

                    $file->move($filesDirectory, $fileName);

                    $this->saveFile($fileName, $contentE);
                }
            }
        }
    }
    
    protected function saveFile($fileName, $contentE) {
        $fileE = new File();
        $fileE->setContent($contentE);
        $fileE->setName($fileName);
            
        $em = $this->getDoctrine()->getManager();

        $em->persist($fileE);

        $em->flush();
    }
    
    /**
     * Matches /api/start exactly
     *
     * @Route("/api/start", name="api_start")
     */
    public function startAction(Request $request)
    {
        if ($request->getMethod() !== 'POST') {
            throw new HttpException(400, 'Nieprawidłowe zapytanie');
        }
        
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:EmailQueue');
        
        $query = $repository->createQueryBuilder('EmailQueue');
        $query->update()
            ->set('EmailQueue.status', ':status_inprogress')
            ->setParameter('status_inprogress', EmailQueue::STATUS_INPROGRESS)
            ->where('EmailQueue.status = :status_new')
            ->setParameter('status_new', EmailQueue::STATUS_NEW);
        $count = $query->getQuery()->execute();
        
        $this->response->setData([
            'status' => 'success',
            'count' => $count
        ]);
        
        return $this->response;
    }    
    
    //////////////////////////////////////////////////////////////////////
    
    /**
     * 
     * @param Request $request
     * @param \AppBundle\Entity\EmailQueue $contentE
     * @return array
     */
    protected function saveEmailQueue(Request $request, $contentE)
    {
        $ids = [];
        $em = $this->getDoctrine()->getManager();

        foreach ($request->request->get('email') as $email) {
            $emailQueueE = new EmailQueue();
            $emailQueueE->setContent($contentE);
            $emailQueueE->setFromEmail($request->request->get('from'));
            $emailQueueE->setEmail($email);
            $emailQueueE->setSubject($request->request->get('subject'));
            $emailQueueE->setPriority($request->request->get('priority'));

            $em->persist($emailQueueE);

            $em->flush();

            $ids[] = $emailQueueId = $emailQueueE->getId();
        }

        return $ids;
    }    
    
    /**
     * 
     * @param Request $request
     * @param string $linkName
     * @param array|null $statuses
     * @return JsonResponse
     */
    protected function listEmails(Request $request, $linkName, $statuses = null)
    {
        
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:EmailQueue');
        
        $query = $repository->createQueryBuilder('EmailQueue');
        
        if (!empty($statuses)) {
            $query->where('EmailQueue.status IN (:statuses)')
                ->setParameter('statuses', $statuses);
        }
        
        $paginatedCollection = $this->get('pagination_factory')
            ->createCollection($query, $request, $linkName);
        
        $this->response->setData([
            'data' => $this->prepareData(
                $paginatedCollection->getItems()
            ),
            'count' => $paginatedCollection->getCount(),
            'total' => $paginatedCollection->getTotal(),
            'links' => $paginatedCollection->getLinks(),
        ]);        
        
        return $this->response;        
    }
    
}